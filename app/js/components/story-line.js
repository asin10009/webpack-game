export default {
    getSolutions: function(idSituation) {
        return this.situations[idSituation].ansvers.map((item) => {
            return this.solutions[item];
        });
    },
    history: [],
    enterPoint: 'a1',
    situations: {
        a1: {
            id: 'a1',
            description: [
                'Здравствуйте ваше величество {{name}} III.',
                'В некотором царстве, в некотором государстве жил был король. А потом умер.',
                'Хорошо что успел оставить потомка. Так что бразды правления государством прийдется освоить тебе, молодой принц.',
                'Королевство у нас хорошее, только казана пуста, да люди мрут часто'
            ],
            ansvers: ['b1', 'b2', 'b3'],
            historyNote:['a1 Летопись1']
        },
        a2: {
            id: ['a2'],
            description: ['Ситуация2'],
            ansvers: ['b4', 'b5', 'b6'],
            historyNote:['a1 Летопись2']
        }
    },
    solutions: {
        b1: {
            id: 'b1',
            description: 'Решение1',
            aftermaths: ['c1'],
            historyNote:['a1 Летопись3']
        },
        b2: {
            id: 'b2',
            description: 'Решение2',
            aftermaths: ['c2'],
            historyNote:['a1 Летопись4']
        },
        b3: {
            id: 'b3',
            description: 'Решение3',
            aftermaths: ['c3'],
            historyNote:['a1 Летопись5']
        },
        b4: {
            id: 'b4',
            description: 'Решение4',
            aftermaths: ['c4'],
            historyNote:['a1 Летопись6']
        },
        b5: {
            id: 'b5',
            description: 'Решение5',
            aftermaths: ['c4'],
            historyNote:['a1 Летопись7']
        },
        b6: {
            id: 'b6',
            description: 'Решение6',
            aftermaths: ['c4'],
            historyNote:['a1 Летопись8']
        }
    },
    aftermaths: {
        c1: {
            id: 'c1',
            description: 'последствие1',
            situation: 'a2',
            historyNote:['a1 Летопись9']
        },
        c2: {
            id: 'c2',
            description: 'последствие2',
            situation: 'a2',
            historyNote:['a1 Летопись10']
        },
        c3: {
            id: 'c3',
            description: 'последствие3',
            situation: 'a2',
            historyNote:['a1 Летопись11']
        },
        c4: {
            id: 'c4',
            description: 'последствие4',
            situation: 'a2',
            historyNote:['a1 Летопись12']
        },
    },
    dates:{
        startYear:512,
        months:['Cечень', 'Вьюговей', 'Птичень', 'Первоцвет', 'Травник', 'Хлеборост', 'Грозник', 'Жнивень', 'Хмурень', 'Грязник', 'Мертволист', 'Студень']
    }
}