import ee from './event-emmiter';
import storyLine from './story-line';

class stateStorage{
	constructor(){	
		this.startState = [{
	            history:[{
	               situation: storyLine.situations[storyLine.enterPoint],
	               selectedAnswer: false,
	               aftermaths:false
	            }],
	            name:''
	    }];
		this.state = this.startState;
		//this.tryLoadGame();
	}
	setState(newState) {
		this.state = state.push(newState);	
	}
	getActualState(){
		console.log(this.state[this.state.length-1]);
		console.log(this.state);
		return this.state[this.state.length-1];
	}
	tryLoadGame(){
        let gameName = 'Kingdom';
        let loadState = JSON.parse(localStorage.getItem('game' + gameName));
        if(loadState!==null){
            this.state = loadState;
        }
    }
    saveGame(){
        let gameName = 'Kingdom';
        localStorage.setItem('game' + gameName, JSON.stringify(this.state));
    }
    doFullRefresh(){
        this.setState(this.startState);
        return this.getActualState();
    }
    goBack(){
        //back to last solution set
        if(this.state.length===1){
            return;
        }else{
        	this.state.pop();
        }
        return this.getActualState();
    }
};

export default new stateStorage();