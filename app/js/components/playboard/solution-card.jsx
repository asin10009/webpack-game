import React from 'react';

export default class SolutionCard extends React.Component {

    constructor() {
        super();

         this.state = {
            selected: false
        };

    }
    shouldComponentUpdate(nextProps, nextState){
        if(nextState.selected){
            return true;
        }
        // console.log('return false');
        return false;
    }
    clickHandler(e){
        if(!this.props.disabled){
            this.props.parentMethods.onClick(this.props.data);
            this.setState({selected: true});
        }else{
            // console.log('disabled');
        }
    }
    render() {
         // console.log('render card');
        	return (
        		<div
                    className="solution-card-wrapper"
                    onClick={this.clickHandler.bind(this)}
                >
                    <div className="solution-card">
        			    {this.props.data.description}
                    </div>
        		</div>
        		);
    }
}