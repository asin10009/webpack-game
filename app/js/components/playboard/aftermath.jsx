import React from 'react';
export default class PlayBoard extends React.Component {
    constructor() {
        super();
    }
    renderButton(){
        if(this.props.showButton){
            return <button onClick={this.props.buttonClick}>Далее</button>;
        }
    }
    render(){
        return(
            <div>
                <div>{this.props.data.description}</div>
                {this.renderButton()}
            </div>
        )
    }
}