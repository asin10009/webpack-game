import React from 'react';

export default class Situation extends React.Component {
    constructor() {
        super();
    }
    shouldComponentUpdate(nextProps, nextState){
        return false;
    }
    renderDescription(){
        return this.props.data.description.map(function(item, i) {
            return (
                <p key={i}>{this.props.parentMethods.formatText(item)}</p>
                )
        }, this);
    }
    render() {
    	return (
    		<div
                className="situation"
            >
    			{this.renderDescription()}
    		</div>
    	);
    }
}