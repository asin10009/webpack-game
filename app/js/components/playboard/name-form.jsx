import React from 'react';

export default class NameForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name:'',
            emptyName:false
        }
    }
    buttonClickHandler(){
        var name = this.refs.nameInput.value.trim();
        if(name === ''){
            this.setState({
                emptyName:true
            });
        }else{
            this.setState({
                name:name
            });
            this.props.parentMethods.setName(name);
        }
    }
    render() {
    	return (
    		<div className={(this.state.name !== ''?'hidden':'') + ' name-form'}>
                <div className={(this.state.emptyName?'':'hidden') + ' name-form__message'}>Вы же король, у вас должно быть имя.</div>
                <input className="input name-form__input" type="text" ref="nameInput"/>
                <button className="name-form__button button" onClick={this.buttonClickHandler.bind(this)}>Принять</button>
            </div>
    	);
    }
}