import React from 'react';
import storyLine from '../story-line';
import SolutionCard from './solution-card.jsx';
import Situation from './situation.jsx';
import Aftermath from './aftermath.jsx';
import NameForm from './name-form.jsx';
import stateStorage from '../state-storage';
// import stateStorage from './state-storage';


export default class PlayBoard extends React.Component {
    constructor() {
        super();

        this.state = stateStorage.getActualState();
        console.log(stateStorage.getActualState());

        this.cardsMethods={
            onClick:this.clickToCard.bind(this)
            //methods for cards
        }

        this.formMethods={
            setName:this.setName.bind(this)
            //methods for form component
        }

        this.situationMethods={
            formatText:this.formatText.bind(this),
        };

    }
    componentDidUpdate(prevProps, prevState){
        stateStorage.setState(this.state);
    }
    // componentWillMount(){
    //     this.tryLoadGame();
    // }
    // componentDidUpdate(){
    //     this.saveGame();
    //     console.log('save game');
    // }
    getCurrentPoint() {
        //get last item from history arr
        return this.state.history[this.state.history.length - 1];
    }
    getAftermath(ansver){
        //return aftermath from storyLine,
        //in feature will creating checks
        return storyLine.aftermaths[ansver.aftermaths[0]];
    }
    setAnsverToHistory(ansver){
        //must be runned when user select ansver, set into history ansver and aftermath 
        let newHistory = this.state.history;
        let historyCurrentPoint = newHistory[newHistory.length-1];
        historyCurrentPoint.selectedAnswer = ansver;
        historyCurrentPoint.aftermaths = this.getAftermath(ansver);

        this.setState({
            history: newHistory
        });

    }
    clickToCard(ansver){
        //click to card handler
        //maby it is ned remove
        this.setAnsverToHistory(ansver);
    }
    renderSolutionComponents(i){
            //return set of solutions
        	let results = storyLine.getSolutions(this.state.history[i].situation.id);

            return results.map(function(item) {
                return <SolutionCard 
                        key={item.id}
                        data={item}
                        parentMethods={this.state.history[i].selectedAnswer ? null : this.cardsMethods }
                        disabled={this.state.history[i].selectedAnswer ? true : false}/>
        	}, this)
    }
    goToNextPoint(){
        //fire after click next button, run app to next step
        console.log(this.state);
        if(!this.getCurrentPoint().selectedAnswer){
            //if user click next, but not select answer, never happens
            console.error('answer not selected');
        }else{
            let history = this.state.history;
            history.push({
                situation: storyLine.situations[this.getCurrentPoint().aftermaths.situation],
            });
            this.setState({
                history: history
            });
        }
    }
    //set property name, this method passed to child
    setName(name){
        this.setState({
            name:name
        });
    }
    //replace str temlate to dinamic state, like name and ather
    formatText(str){
        str = str.replace('{{name}}', this.state.name);
        return str;
    }
    //replace str tmp to state for Array
    formatTextArray(strArray){
        return strArray.map(function(str) {
            return this.formatText(str);
        })
    }
    renderAftermath(i){
        //render aftermath, or null, or ftermath and next-button
        if(this.state.history[i].selectedAnswer){

            let aftermath = storyLine.aftermaths[this.state.history[i].selectedAnswer.aftermaths];

            return <Aftermath
                buttonClick={this.goToNextPoint.bind(this)}
                data={aftermath}
                showButton={i === this.state.history.length-1}
                />;
        }else{
            return null;
        }
    }
    // saveGame(){
    //     let gameName = 'Kingdom';
    //     localStorage.setItem('game' + gameName, JSON.stringify(this.state));
    // }
    // tryLoadGame(){
    //     let gameName = 'Kingdom';
    //     var loadState = JSON.parse(localStorage.getItem('game' + gameName));
    //     if(loadState!==null){
    //         this.setState(loadState);
    //     }
    // }
    // doFullRefresh(){
    //     this.setState(this.startState);
    // }
    renderHistory(){
        //main render method, render history array  
        return this.state.history.map(function(point, i){
            return (
                <div className="history-point" key={point.situation.id+i}>
                    <Situation parentMethods={this.situationMethods} data={point.situation}></Situation>
                    <div className="card-set">
                        {this.renderSolutionComponents(i)}
                    </div>
                    {this.renderAftermath(i)}
                </div>
            )
        }, this)
    }
    render() {
        console.log('render playboard');
        //will be rendered form if name empty
        if(this.state.name!==''){
            return (
                <div className="play-board">
                    <div className="play-board__inner-container">
                        {this.renderHistory()}
                    </div>
                    <div>
                        <button onClick={stateStorage.saveGame.bind(stateStorage)}>Сохранить</button>
                        <button >На шаг назад</button>
                        <button >Рестарт</button>
                    </div>
                </div>
            )
        }else{
            return (
                <div className="play-board">
                    <div className="play-board__inner-container">
                        <NameForm parentMethods={this.formMethods}/>
                    </div>
                </div>
            )
        }
    }
}

// onClick={this.doFullRefresh.bind(this)}
// onClick={this.goBack.bind(this)}