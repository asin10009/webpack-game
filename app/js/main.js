import ReactDOM from 'react-dom';
import React from 'react';
// import Styles from '../scss/main.scss';
import PlayBoard from './components/playboard/playboard.jsx';
import Chronicle from './components/chronicle/chronicle.jsx';
import { Router, Route, IndexRoute, browserHistory, Link } from 'react-router';
// import { Router, Route, IndexRoute, hashHistory , Link } from 'react-router';


class App extends React.Component {
    constructor() {
        super();

        this.state = {};
        this.playboardMethods={
            triggerStateChange: this.setStateManual.bind(this)
        }
    }
    // shouldComponentUpdate(){
    //     return false;
    // }
    setStateManual(newState){
        this.setState(newState);
    }
    render() {
        return(
            <div className="main-wrapper">
                <ul>
                  <li><Link activeClassName="active" to="/">Дела</Link></li>
                  <li><Link activeClassName="active" to="/chronicle">Летопись</Link></li>
                  <li><Link activeClassName="active" to="/dasaddaads/asdasdas">111</Link></li>
                </ul>
                {this.props.children}
            </div>
        )
            //return null;
        	// {storyLine.situations.a1.description}
        	//console.log(111232);
        	
    }
}

ReactDOM.render(
 <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={PlayBoard} />
      <Route path='chronicle' component={Chronicle}/>
      <Route path='*' component={PlayBoard} />
    </Route>
  </Router>
, document.getElementById("react-conteiner"));