var path = require('path');
var webpack = require('webpack');
//var ExtractTextPlugin = require('extract-text-webpack-plugin');


var config = {
    //context: __dirname,
    entry:{
        'js':'./app/js/main.js',
    },
    watch: true,
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build'),
    },
    // plugins: [
    //     new ExtractTextPlugin('styles.css', {
    //         allChunks: true
    //     })
    // ],
    module: {
        loaders: [{
            test: [/.jsx$/, /.js$/],
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                presets: ['es2015', 'react']
            }  
        }
        // ,{
        //     test: /\.scss$/,
        //     loader: ExtractTextPlugin.extract('style-loader', 'css-loader?sourceMap!postcss-loader!resolve-url!sass-loader?sourceMap')
        // },{
        //     test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
        //     loader: 'file-loader'
        // }
        ]
    },
    devtool: "sourcemap",
    // devServer:{
    //     contentBase: path.join(__dirname, "build"),
    //     port: 9000,
    //     inline: true
    // }
};
module.exports = config;