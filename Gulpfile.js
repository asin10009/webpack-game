'use strict';

var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create();


// Scss stylesheets
gulp.task('stylesheets', function() {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            //includePaths: bourbon.includePaths
        })).on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(gulp.dest('build/'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function() {
    watch(['app/scss/**/*.scss'], function(event, cb) {
        gulp.start('stylesheets');
    });

    gulp.watch("./build/*.*").on('change', browserSync.reload);
});

gulp.task('serve', function() {
    browserSync.init({
        server: "./build/",
        open: true,
        port: 9000
    });

});

// Run
gulp.task('default', [
    'stylesheets',
    'serve',
    'watch'
]);
